const int kLogDebugLevel = Log.Info;

class Log {
  static const int Error = 0;
  static const int Debug = 1;
  static const int Info = 2;

  static const String _greyColorCode = '\u001b[90m';
  static const String _redColorCode = '\u001b[31m';
  static const String _magentaColorCode = '\u001b[35m';
  static const String _cyanColorCode = '\u001b[36m';
  static const String _resetColorCode = '\u001b[0m';

  /// return [tag] if it is string else return type of tag as string
  static String _getTag(tag) =>
      (tag.runtimeType == String ? tag : tag.runtimeType).toString();

  /// return [msg] if it is string else calls toString method
  static String _getMsg(msg) =>
      msg.runtimeType == String ? msg : msg.toString();

  /// returns current formatted time
  static String _curTime() => DateTime.now().toString();

  /// static method to print debug logs of eventizer Package
  /// format is simple time, type of [tag] or [tag] if it is strign, message [msg]
  static e(dynamic tag, dynamic msg) {
    if (Log.Error <= kLogDebugLevel) {
      print(
          '$_magentaColorCode${_curTime()}: $_redColorCode${_getTag(tag)}:\t${_getMsg(msg)}$_resetColorCode');
    }
  }

  /// static method to print debug logs of eventizer Package
  /// format is simple time, type of [tag] or [tag] if it is strign, message [msg]
  static d(dynamic tag, dynamic msg) {
    if ((Log.Debug <= kLogDebugLevel)) {
      print(
          '$_magentaColorCode${_curTime()}: $_cyanColorCode${_getTag(tag)}:\t${_getMsg(msg)}$_resetColorCode');
    }
  }

  /// static method to print debug logs of eventizer Package
  /// format is simple time, type of [tag] or [tag] if it is strign, message [msg]
  static i(dynamic tag, dynamic msg) {
    if ((Log.Info <= kLogDebugLevel)) {
      print(
          '$_magentaColorCode${_curTime()}: $_greyColorCode${_getTag(tag)}:\t${_getMsg(msg)}$_resetColorCode');
    }
  }

  /// static method to print debug logs of eventizer Package
  /// format is simple time, type of [tag] or [tag] if it is strign, message [msg]
  static p(int logLevel, dynamic tag, String msg) {
    if (logLevel <= kLogDebugLevel) {
      switch (logLevel) {
        case Log.Error:
          print(
              '$_magentaColorCode${_curTime()}: $_redColorCode${_getTag(tag)}:\t${_getMsg(msg)}$_resetColorCode');
          break;
        case Log.Debug:
          print(
              '$_magentaColorCode${_curTime()}: $_cyanColorCode${_getTag(tag)}:\t${_getMsg(msg)}$_resetColorCode');
          break;
        case Log.Info:
          print(
              '$_magentaColorCode${_curTime()}: $_greyColorCode${_getTag(tag)}:\t${_getMsg(msg)}$_resetColorCode');
          break;
      }
    }
  }
}
