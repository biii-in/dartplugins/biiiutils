extension ListEx<T> on List<T> {
  List<Y> transform<Y>(Y transformFunction(T e, int index)) {
    List<Y> newList = [];
    int index = 0;
    this.forEach((element) => newList.add(transformFunction(element, index++)));
    return newList;
  }
}
